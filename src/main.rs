mod vector;
pub use vector::cross;
pub use vector::dot;
pub use vector::Vec3;

mod ray;
pub use ray::Ray;

fn hit_shere(center: &Vec3, radius: f32, ray: &Ray) -> bool {
    let oc = ray.origin - *center;
    let a = dot(&ray.direction, &ray.direction);
    let b = 2.0 * dot(&oc, &ray.direction);
    let c = dot(&oc, &oc) - radius * radius;
    let discriminant = b * b - 4.0 * a * c;
    discriminant > 0.0
}

fn ray_color(ray: &Ray) -> Vec3 {
    if hit_shere(&Vec3::new(0., 0., -1.), 0.5, ray) {
        Vec3::new(1., 0., 0.)
    } else {
        let unit_direction = ray.direction.unit_vector();
        let t = 0.5 * (unit_direction.y + 1.0);
        (1.0 - t) * Vec3::new(1.0, 1.0, 1.0) + t * Vec3::new(0.5, 0.7, 1.0)
    }
}

fn main() {
    const IMAGE_WIDTH: u32 = 200;
    const IMAGE_HEIGHT: u32 = 100;

    println!("P3\n{} {}\n255", IMAGE_WIDTH, IMAGE_HEIGHT);

    let lower_left_coner = Vec3::new(-2., -1., -1.);
    let horizontal = Vec3::new(4., 0., 0.);
    let vertical = Vec3::new(0., 2., 0.);
    let origin = Vec3::zero();

    for j in (0..IMAGE_HEIGHT).rev() {
        eprintln!("\rScanlines remaining: {}", j);
        for i in 0..IMAGE_WIDTH {
            let u = i as f32 / IMAGE_WIDTH as f32;
            let v = j as f32 / IMAGE_HEIGHT as f32;

            let u_horizontal = u * horizontal;
            let v_vertical = v * vertical;
            let direction = lower_left_coner + u_horizontal + v_vertical;

            let r = Ray::new(origin, direction);

            let color = ray_color(&r);

            color.write_color();
        }
    }
    eprintln!("\nDone.");
}
